from django import forms
from myapps.ventas.models import Ventas,DetalleVentas
from django.utils import timezone
import datetime

#Formulario para realizar una venta
class VentasForm(forms.ModelForm):
	class Meta:
		model = Ventas

		fields = [
			'fecha',
			'carnet',
		]

		labels = {
			'fecha' : 'Fecha',
			'carnet' : 'Carnet Mascota',
		}

		widgets = {
			'fecha' : forms.SelectDateWidget(),
			'carnet' : forms.Select(attrs={'class':'form-control'}),
		}

class DetalleVentasForm(forms.ModelForm):
	class Meta:
		model = DetalleVentas
		fields = ['servicio']

		labels = {'servicio' : 'Servicio'}

		widgets = {
			'servicio' : forms.Select(attrs={'class':'form-control'}),
		}


