# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy
from django.contrib import messages
from django.utils import timezone
from datetime import datetime
#Vistas genericas
from django.forms.formsets import formset_factory, BaseFormSet
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
#Forms
from myapps.ventas.forms import VentasForm, DetalleVentasForm
from myapps.ventas.models import Ventas, DetalleVentas
from myapps.carnet.models import DetalleCarnet, Carnet
#Vista de carnet 
from myapps.carnet.views import servicio_nuevo_echo
from myapps.servicios.views import verificar_Existencias, descargarInsumoServicio

from myapps.general.views import Configuraciones
from decimal import Decimal
# Create your views here.

#Vista de prueba
def index(request):
	return HttpResponse('App de Ventas')

#Vista para insertar a la BD
def venta_create(request):
	TodoItemFormSet = formset_factory(DetalleVentasForm, max_num=100, formset=BaseFormSet, extra=1)
	if request.method == 'POST':
		form = VentasForm(request.POST)
		form2 = TodoItemFormSet(request.POST,request.FILES)
		if form.is_valid() and form2.is_valid():
			nueva_venta = form.save(commit=False)
			cantif = 0
			preciof = 0 
			nueva_venta.save()
			n = form.save()
			for f in form2.forms:
				item = f.save(commit=False)
				item.venta = n
				item.save()
				cantif = cantif + 1
				preciof = preciof + item.servicio.precio
				#Se crea el detalle carnet
				servicio_nuevo_echo(n,item)
				#Se descarga de inventario
				descargarInsumoServicio(item.servicio.id)

			nueva_venta.cantidad = cantif
			nueva_venta.total = preciof
			nueva_venta.save()
		return redirect('ventas:ventas_listar')
	else:
		form = VentasForm()
		form2 = TodoItemFormSet()
	nom = Configuraciones.objects.get(pk=1)
	c = {'form': form,'form2': form2,'nombreE':nom.nombre}

	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	if tipo == "Q":
		return render(request, 'ventas/ventas_form.html',c)
	if tipo == "D":
		return render(request, 'ventas/ventas_formD.html',c)
	if tipo == "E":
		return render(request, 'ventas/ventas_formE.html',c)
	else:
		return render(request, 'ventas/ventas_form.html',c)

#Vista para realizar un servicio segun uno programado anteriormente

#Vista para listar todas las ventas
def ventas_list(request):
	ventas = Ventas.objects.all().order_by('-fecha')
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	nom = Configuraciones.objects.get(pk=1)
	if(tipo == "D"):
		ventas = convertiraDolares(ventas)
		contexto = {'ventas':ventas,'nombreE':nom.nombre}
		return render(request,'ventas/ventas_listD.html',contexto)
	if(tipo == "E"):
		ventas = convertirEuros(ventas)
		contexto = {'ventas':ventas,'nombreE':nom.nombre}
		return render(request,'ventas/ventas_listE.html',contexto)
	contexto = {'ventas':ventas,'nombreE':nom.nombre}
	return render(request,'ventas/ventas_list.html',contexto)

def convertiraDolares(ventas):
	for v in ventas:
		v.total = round((v.total / Decimal(7.72)),2)
	return ventas

def convertirEuros(ventas):
	for v in ventas:
		v.total = round((v.total / Decimal(8.87)),2)
	return ventas	


#Vista para listar el detalle de las ventas
def detalle_ventas(request,pk):
	v = Ventas.objects.filter(pk=pk)
	v = v.iterator().next()
	detalles = DetalleVentas.objects.filter(venta=pk)
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	nom = Configuraciones.objects.get(pk=1)
	if(tipo == "D"):
		detalles = detalleDolares(detalles)
		contexto = {'detalles':detalles,'total': round((v.total / Decimal(7.72)),2),'nombreE':nom.nombre}
		return render(request,'ventas/ventas_detalleD.html',contexto)
	if tipo == "E":
		detalles = detalleEuros(detalles)
		contexto = {'detalles':detalles,'total':round((v.total / Decimal(8.87)),2),'nombreE':nom.nombre}
		return render(request,'ventas/ventas_detalleE.html',contexto)
	contexto = {'detalles':detalles,'total':v.total,'nombreE':nom.nombre}
	return render(request,'ventas/ventas_detalle.html',contexto)

def detalleDolares(detalle):
	for d in detalle:
		d.servicio.precio = round((d.servicio.precio / Decimal(7.72)),2)
	return detalle

def detalleEuros(detalle):
	for d in detalle:
		d.servicio.precio = round((d.servicio.precio / Decimal(8.87)),2)
	return detalle

def venta_programada(request,pk):
	detalleC = DetalleCarnet.objects.filter(pk=pk)
	detalleC = detalleC.iterator().next()
	carnet = Carnet.objects.filter(pk = detalleC.carnet.id)
	carnet = carnet.iterator().next()
	v = Ventas(
		#fecha = timezone.now(),
		fecha=datetime.today(),
		cantidad = 1,
		total = detalleC.servicio.precio,
		carnet = carnet
	)
	v.save()
	print v
	detalle = DetalleVentas(
		venta = v,
		servicio = detalleC.servicio,
	)
	detalle.save()
	detalleC.estado = True
	detalleC.save()
	return redirect('ventas:ventas_listar')
