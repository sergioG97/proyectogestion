from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.ventas.views import index, venta_create, ventas_list, detalle_ventas,venta_programada


urlpatterns = [
	url(r'^$', index),
	url(r'^nuevo', login_required(venta_create), name='ventas_crear'),
	url(r'^listar', login_required(ventas_list), name='ventas_listar'),
	url(r'^detalle/(?P<pk>\d+)$', login_required(detalle_ventas), name='ventas_detalle'),
	url(r'^programada/(?P<pk>\d+)$', login_required(venta_programada), name='ventas_programadas'),
]