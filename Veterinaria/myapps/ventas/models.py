# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from myapps.carnet.models import Carnet
from myapps.servicios.models import Servicios
# Create your models here.

#Modelo para la tabla ventas
class Ventas(models.Model):
	fecha = models.DateField()
	cantidad = models.IntegerField(default = 0)
	total = models.DecimalField(max_digits = 7,decimal_places=2,default=0)
	carnet = models.ForeignKey(Carnet,null=True,blank=True)

class DetalleVentas(models.Model):
	venta = models.ForeignKey(Ventas,null=True,blank=True)
	servicio = models.ForeignKey(Servicios,null=True,blank=True)
