# -*- coding: utf-8 -*-
# Generated by Django 1.11 on 2018-11-13 02:23
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('planilla', '0004_auto_20181028_1920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='empleados',
            name='genero',
            field=models.CharField(choices=[('M', 'Masculino'), ('F', 'Femenino'), ('N', 'No especifica')], max_length=20),
        ),
        migrations.AlterField(
            model_name='planilla',
            name='mes',
            field=models.CharField(choices=[('Enero', 'Enero'), ('Febrero', 'Febrero'), ('Marzo', 'Marzo'), ('Abril', 'Abril'), ('Mayo', 'Mayo'), ('Junio', 'Junio'), ('Julio', 'Julio'), ('Agosto', 'Agosto'), ('Septiembre', 'Septiembre'), ('Octubre', 'Octubre'), ('Noviembre', 'Noviembre'), ('Diciembre', 'Diciembre')], max_length=10),
        ),
    ]
