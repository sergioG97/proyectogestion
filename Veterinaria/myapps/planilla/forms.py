from django import forms
from myapps.planilla.models import Planilla
from myapps.planilla.models import Empleados
#Formulario de Inventario
#Este formulario es para ingresar nuevos insumos a la tabla Inventario
#Importante para el nombre de esta clase la nomenclatura sera Nombre del Modelo - Form
class PlanillaForm(forms.ModelForm):
	class Meta:
		model = Planilla

		#Campos del Modelo
		fields = [
			'mes',
		#	'anio',
			'total_planilla',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'mes' : 'Mes de planilla',
			'anio' : 'Anio de la planilla',
			'total_planilla' : 'Total planilla del mes',
		}

		#Tipos de Campos para ingresar datos ej: TextBox, Combobox, checkbox, etc
		#Va el nombre del campo : Tipo de campo
		widgets = {
			'mes' : forms.Select(attrs={'class':'form-control'}),
			'anio' : forms.Select(attrs={'class':'form-control'}),
			'total_planilla' : forms.TextInput(attrs={'class':'form-control'}),
		}


class EmpleadosForm(forms.ModelForm):
	class Meta:
		model = Empleados
		#genero = (('M','Masculino'),('F','Femenino'),('N','No especifica'))
		genero = ['M','F','N']
		#Campos del Modelo
		fields = [
			'nombre',
			'apellidos',
			'telefono',
			'dpi',
			'edad',
			'genero',
			'email',
			'sueldo',		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'nombre' : 'Nombres',
			'apellidos' : 'Apellidos',
			'telefono' : 'Telefono',
			'dpi' : 'No. de DPI',
			'edad' : 'Edad',
			'genero' : 'Genero',
			'email' : 'Correo Electronico',
			'sueldo' : 'Sueldo en quetzales',
		}

		#Tipos de Campos para ingresar datos ej: TextBox, Combobox, checkbox, etc
		#Va el nombre del campo : Tipo de campo
		widgets = {
			'nombre' : forms.TextInput(attrs={'class':'form-control'}),
			'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
			'telefono' : forms.TextInput(attrs={'class':'form-control'}),
			'dpi' : forms.TextInput(attrs={'class':'form-control'}),
			'edad' : forms.TextInput(attrs={'class':'form-control'}),
			'genero' : forms.Select(attrs={'class':'form-control'}),
			#'genero' : forms.ChoiceField(choices=genero),
			'email' : forms.TextInput(attrs={'class':'form-control'}),
			'sueldo' : forms.TextInput(attrs={'class':'form-control'}),
		}
