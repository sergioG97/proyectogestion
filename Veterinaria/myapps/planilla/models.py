# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

#Modelo para la tabla Planilla
class Planilla(models.Model):
	meses = (('Enero','Enero'),('Febrero','Febrero'),('Marzo','Marzo'),('Abril','Abril'),('Mayo','Mayo'),('Junio','Junio'),
			 ('Julio','Julio'),('Agosto','Agosto'),('Septiembre','Septiembre'),('Octubre','Octubre'),('Noviembre','Noviembre'),
			 ('Diciembre','Diciembre'))
	datos = (('2018','2018'),('2019','2019'))
	mes = models.CharField(max_length=10,choices=meses)
	total_planilla = models.DecimalField(max_digits = 7,decimal_places=2)

#Modelo para la tabla Empleados
class Empleados(models.Model):
	genero = (('M','Masculino'),('F','Femenino'),('N','No especifica'))
	nombre = models.CharField(max_length = 45)
	apellidos = models.CharField(max_length = 45)
	telefono = models.CharField(max_length = 15)
	dpi = models.CharField(max_length = 15)
	edad = models.IntegerField()
	genero = models.CharField(max_length = 20,choices=genero)
	email = models.EmailField()
	sueldo = models.DecimalField(max_digits = 7,decimal_places=2)
	planilla_id = models.ForeignKey(Planilla, null=True, blank=True)

#Aca irian los usuarios
