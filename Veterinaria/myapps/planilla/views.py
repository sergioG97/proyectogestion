# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
#Para vistas basadas en clases
from myapps.planilla.models import Planilla, Empleados
from myapps.planilla.forms import PlanillaForm, EmpleadosForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView


#Vista de prueba
def index(request):
	return render(request,'planilla/index.html')

def planilla_list(request):
	planilla = Planilla.objects.all().order_by('id')#para odenar por id :)
	contexto = {'planilla':planilla}
	return render(request,'planilla/planilla_list.html',contexto)

def planilla_create(request):
	if request.method == 'POST':
		form = PlanillaForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('planilla:planilla_listar')
	else:
		form = PlanillaForm()

	return render(request, 'planilla/planilla_form.html',{'form':form})

def empleados_list(request):
	empleados = empleados.objects.all().order_by('id')
	contexto = {'planilla':empleados}
	return render(request,'planilla/empleados_list.html',contexto)

def empleados_create(request):
	if request.method == 'POST':
		form = EmpleadoForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('planilla:empleados_listar')
	else:
		form = EmpleadosForm()

	return render(request, 'planilla/empleados_form.html',{'form':form})

#Vista para crear insumos en Inventario
class PlanillaCreate(CreateView):
	model = Planilla
	form_class = PlanillaForm
	template_name = 'planilla/planilla_form.html'
	success_url = reverse_lazy('planilla:planilla_listar')

#Vista para listar los insumos del Inventario
class PlanillaList(ListView):
	model = Planilla
	template_name = 'planilla/planilla_list.html'


class EmpleadosCreate(CreateView):
	model = Empleados
	form_class = EmpleadosForm
	template_name = 'planilla/empleados_form.html'
	success_url = reverse_lazy('planilla:empleados_listar')

#Vista para listar los insumos del Inventario
class EmpleadosList(ListView):
	model = Empleados
	template_name = 'planilla/empleados_list.html'

class EmpleadosUpdate(UpdateView):
	model = Empleados
	form_class = EmpleadosForm
	template_name = 'planilla/empleados_form.html'
	success_url = reverse_lazy('planilla:empleados_listar')


