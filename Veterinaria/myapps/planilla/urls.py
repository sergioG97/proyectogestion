from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.planilla.views import index, planilla_create ,PlanillaList, PlanillaCreate
from myapps.planilla.views import empleados_create,EmpleadosList,EmpleadosCreate, EmpleadosUpdate

#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$', index),
    #Expresion regular, Vista , nombre de la vista
    url(r'^nuevo', login_required(PlanillaCreate.as_view()), name='planilla_crear'),
    url(r'^listar', login_required(PlanillaList.as_view()), name='planilla_listar'),
    url(r'^empleadolistar', login_required(EmpleadosList.as_view()), name='empleados_listar'),
    url(r'^empleado', login_required(EmpleadosCreate.as_view()), name='empleados_crear'),
    url(r'^editar_empleado/(?P<pk>\d+)$',login_required(EmpleadosUpdate.as_view()), name='empleado_editar'),
]