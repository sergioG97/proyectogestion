# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from myapps.inventario.models import Inventario
# Create your models here.

class Servicios(models.Model):
	tipo_servicio = models.CharField(max_length=45)
	precio = models.DecimalField(max_digits = 7,decimal_places=2)
	def __unicode__(self):
		return '{}'.format(self.tipo_servicio)

class DetalleServicios(models.Model):
	servicio = models.ForeignKey(Servicios,null=True,blank=True)
	inventario = models.ForeignKey(Inventario,null=True,blank=True)
	cantidad = models.IntegerField()

