from django import forms
from myapps.servicios.models import Servicios, DetalleServicios

#Formulario para ingresar nuevos servicios a la BD
#De momento sin sus detalles
class ServicioForm(forms.ModelForm):
	class Meta:
		model = Servicios

		#Campos del Modelo
		fields = [
			'tipo_servicio',
			'precio',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'tipo_servicio' : 'Tipo de Servicio',
			'precio' : 'Precio',
		}

		widgets = {
			'tipo_servicio' : forms.TextInput(attrs={'class':'form-control'}),
			'precio' : forms.NumberInput(attrs={'class':'form-control','min' :  '0','placeholder':''}),
		}


#Formulario para ingresar nuevos detalles de los servicios
class DetalleForm(forms.ModelForm):
	class Meta:
		model = DetalleServicios
		#Campos del Modelo
		fields = [
			'inventario',
			'cantidad',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'inventario' : 'Insumo',
			'cantidad' : 'Cantidad',
		}

		widgets = {
			'inventario' : forms.Select(attrs={'class':'form-control'}),
			'cantidad' : forms.NumberInput(attrs={'class':'form-control','min' :  '0'}),
		}


#Formulario para editar detalle de un servicio
class DetalleEditForm(forms.ModelForm):
	class Meta:
		model = DetalleServicios
		#Campos del Modelo
		fields = [
			'inventario',
			'cantidad',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'inventario' : 'Insumo',
			'cantidad' : 'Cantidad',
		}

		widgets = {
			'inventario' : forms.TextInput(attrs={'class':'form-control','readonly':'readonly'}),
			'cantidad' : forms.NumberInput(attrs={'class':'form-control','min' :  '0'}),
		}


