# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect, render_to_response
from django.http import HttpResponse, HttpResponseRedirect, Http404, JsonResponse

from django.core.urlresolvers import reverse_lazy
from django.forms.formsets import formset_factory, BaseFormSet
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from myapps.servicios.models import Servicios, DetalleServicios
from myapps.servicios.forms import ServicioForm, DetalleForm, DetalleEditForm
import json
from myapps.inventario.views import existencias_Inventario, descargar_Inventario
from myapps.general.models import Configuraciones

from decimal import Decimal
# Create your views here.

#Vista de prueba
def index(request):
	return HttpResponse('App de Servicios')

#Vistas para listar servicios
class ServiciosList(ListView):
	model = Servicios
	template_name = 'servicios/servicios_list.html'

def servicios_list(request):
	servicios = Servicios.objects.all().order_by('id')#para odenar por id :)
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	nom = Configuraciones.objects.get(pk=1)
	if tipo == "D":
		servicios = convertiraDolares(servicios)
		contexto = {'object_list':servicios,'nombreE':nom.nombre}
		return render(request,'servicios/servicios_listD.html',contexto)
	if tipo == "E":
		servicios = convertirEuros(servicios)
		contexto = {'object_list':servicios,'nombreE':nom.nombre}
		return render(request,'servicios/servicios_listE.html',contexto)
	
	contexto = {'object_list':servicios,'nombreE':nom.nombre}
	return render(request,'servicios/servicios_list.html',contexto)

def convertiraDolares(servicios):
	for v in servicios:
		v.precio = round((v.precio / Decimal(7.72)),2)
	return servicios

def convertirEuros(servicios):
	for v in servicios:
		v.precio = round((v.precio / Decimal(8.87)),2)
	return servicios

#Metodo para la vista del detalle de un servicio
def detalle_servicios(request,pk):
	try:
		detalle = DetalleServicios.objects.filter(servicio=pk)
		#detalle = DetalleServicios.objects.get(pk=pk)
	except DetalleServicios.DoesNotExist:
		raise Http404("No existen detalles para este servicio")
	nom = Configuraciones.objects.get(pk=1)
	contexto = {'detalles':detalle,'nombreE':nom.nombre}
	return render(request,'servicios/servicios_detalle.html',contexto)

def detalleDolares(detalle):
	for d in detalle:
		d.inventario.precio = round((d.inventario.precio / Decimal(7.72)),2)
	return detalle

def detalleEuros(detalle):
	for d in detalle:
		d.inventario.precio = round((d.inventario.precio / Decimal(8.87)),2)
	return detalle

#Metodo para la vista de crear un servicio con su respectivo detalle
def servicios_create(request):
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	TodoItemFormSet = formset_factory(DetalleForm, max_num=100, formset=BaseFormSet, extra=1)
	if request.method == 'POST':
		form = ServicioForm(request.POST)
		form2 = TodoItemFormSet(request.POST,request.FILES)
		if form.is_valid() and form2.is_valid:
			nuevo_evento = form.save(commit=False)
			#nuevo_evento.precio = 20
			print "Precio1 " , nuevo_evento.precio
			if tipo == "D":
				nuevo_evento.precio = round((nuevo_evento.precio * Decimal(7.72)),2)
			if tipo == "E":
				nuevo_evento.precio = round((nuevo_evento.precio * Decimal(8.87)),2)
			print "Precio1 " , nuevo_evento.precio
			nuevo_evento.save()
			todo_list = form.save()
			for f in form2.forms:
				item = f.save(commit=False)
				item.servicio = todo_list
				item.save()
		return redirect('servicios:servicios_listar')
	else:
		form = ServicioForm()
		form2 = TodoItemFormSet()
	nom = Configuraciones.objects.get(pk=1)
	c = {'form': form,'form2': form2,'nombreE':nom.nombre}
	#tipo = Configuraciones.objects.get(pk=1)
	#tipo = tipo.tipo_cambio
	if tipo == "D":
		return render(request, 'servicios/servicios_formD.html',c)
	if tipo == "E":
		return render(request, 'servicios/servicios_formE.html',c)
	else:
		return render(request, 'servicios/servicios_form.html',c)


#Metodo para la vista de editar un servicio con su respectivo detalle (Tambien editable)
#Vista para editar la info del servicio
class ServiciosUpdate(UpdateView):
	model = Servicios
	form_class = ServicioForm
	template_name = 'servicios/servicios_update.html'
	success_url = reverse_lazy('servicios:servicios_listar')

	def get(self,request,*args,**kwargs):
		#Se obtiene el objeto Servicio
		self.object = self.get_object()
		tipo = Configuraciones.objects.get(pk=1)
		tipo = tipo.tipo_cambio

		if tipo == "D":
			self.object.precio = round((self.object.precio / Decimal(7.72)),2)
		if tipo == "E":
			self.object.precio = round((self.object.precio / Decimal(8.87)),2)

		#Obtenemos el formulario
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		#Obtenemos los detalles del servicio
		detalles = DetalleServicios.objects.filter(servicio=self.object).order_by('pk')
		detalles_data = []
		#Se guarda en un diccionario
		for detalle in detalles:
			d = {'inventario': detalle.inventario,
			'cantidad': detalle.cantidad,'precio':detalle.inventario.precio}
			detalles_data.append(d)
		#se crea el formset con el diccionario obtenido
		DetalleFormSet = formset_factory(DetalleForm, max_num=100, formset=BaseFormSet, extra=0)
		form2 = DetalleFormSet(initial=detalles_data)
		nom = Configuraciones.objects.get(pk=1)
		return self.render_to_response(self.get_context_data(form=form,form2=form2,nombreE=nom.nombre))

	def post(self,request,*args,**kwargs):
		#Obtenemos el objeto servicio
		self.object = self.get_object()
		form_class = self.get_form_class()
		form = self.get_form(form_class)
		detalleS = formset_factory(DetalleForm, max_num=100, formset=BaseFormSet, extra=0)
		form2 = detalleS(request.POST,request.FILES)
		#Verificamos que los formularios sean validos
		if form.is_valid() and form2.is_valid():
			return self.form_valid(form, form2)
		else:
			return self.form_invalid(form, form2)

	def form_valid(self,form,form2):
		#Guardamos el objeto servicio
		nuevo_evento = form.save(commit=False)

		tipo = Configuraciones.objects.get(pk=1)
		tipo = tipo.tipo_cambio
		if tipo == "D":
			nuevo_evento.precio = round((nuevo_evento.precio * Decimal(7.72)),2)
		if tipo == "E":
			nuevo_evento.precio = round((nuevo_evento.precio * Decimal(8.87)),2)
		nuevo_evento.save()
		todo_list = form.save()
		#Eliminamos todas los detalles del servicio
		DetalleServicios.objects.filter(servicio = self.object).delete()
		#Guardamos los nuevos detalles del servicio
		for f in form2.forms:
			item = f.save(commit=False)
			item.servicio = todo_list
			item.save()
		#Se redirecciona a la vista listar
		return HttpResponseRedirect(self.success_url)

	def form_invalid(self,form,form2):
		#Renderizamos los errores
		return self.render_to_response(self.get_context_data(form=form,
                                                         form2 = form2))


#Vista para verificar la existenia de todos los insumos
#De un servicio
#Retorna True si hay existencias, false si no hay existencias suficientes para realizar la venta
def verificar_Existencias(lista,producto_id):
	canti = 0
	for x in lista:
		detalle = DetalleServicios.objects.filter(servicio=x)
		for d in detalle:
			if(d.inventario.id==producto_id):
				c = d.cantidad
				canti = canti + c
				break
	#fin del ciclo
	if(existencias_Inventario(producto_id)<canti):
		return False
	else:
		return True


def existencia_servicio(request):
	canti = 0
	datos = request.GET.getlist('datos[]')
	lista = json.loads(datos[0])
	Id = request.GET.get('id')
	bandera = True
	detalle = DetalleServicios.objects.filter(servicio=Id)
	for d in detalle:
		#if(existencias_Inventario(d.inventario.id)<d.cantidad):
		if(verificar_Existencias(lista,d.inventario.id)==False):
			bandera = False
	nombre = detalle.iterator().next().servicio.tipo_servicio
	response = JsonResponse({'resp': bandera,'nombre':nombre})
	return HttpResponse(response.content)


def porInsumo(detalleV,insumo):
	cantidad = 0
	for f in detalleV:
		detalle = DetalleServicios.objects.filter(servicio=f)
		for d in detalle:
			if d.inventario==insumo:
				cantidad = cantidad + d.inventario.cantidad
	if(existencias_Inventario(insumo.id)<cantidad):
		return False
	return True

def descargarInsumoServicio(idServicio):
	detalle = DetalleServicios.objects.filter(servicio=idServicio)
	for d in detalle:
		descargar_Inventario(d.inventario.id,d.cantidad)


def obtener_precio_ajax(request):
	Id = request.GET.get('id')
	servi = Servicios.objects.filter(pk=Id)
	servi = servi.iterator().next()
	valor = tipo_moneda(servi.precio)
	response = JsonResponse({'precio': valor})
	print servi.precio
	return HttpResponse(response.content)
	#return JsonResponse(response)
	#return render(request, 'ventas/ventas_form.html', {'precio': servi.precio})


def tipo_moneda(precio):
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	if tipo == "Q":
		print "Quetzales"
		return precio
	if tipo == "D":
		print "Dolares"
		return round((precio / Decimal(7.72)),2)
	if tipo == "E":
		print "Euros"
		return round((precio / Decimal(8.87)),2)
	else:
		return precio



