from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.servicios.views import index, servicios_list, servicios_create, ServiciosList, \
 									detalle_servicios,ServiciosUpdate,obtener_precio_ajax,\
 									existencia_servicio

#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$', index),
    url(r'^listar', login_required(servicios_list), name='servicios_listar'),
    url(r'^nuevo', login_required(servicios_create), name='servicios_crear'),
    url(r'^detalle/(?P<pk>\d+)$', login_required(detalle_servicios), name='servicios_detalle'),
    url(r'^editar/(?P<pk>\d+)$', login_required(ServiciosUpdate.as_view()), name='servicios_editar'),
    url(r'^ajax/get_precio/$', obtener_precio_ajax, name='get_precio'),
    url(r'^ajax/get_existencia/$', existencia_servicio, name='get_existencia'),
]