# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.


class Configuraciones(models.Model):
	g = (('Q','Quetzales'),('D','Dolares'),('E','Euros'))
	nombre = models.CharField(max_length=45)
	tipo_cambio = models.CharField(max_length=1,choices=g)


