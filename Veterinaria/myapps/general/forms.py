from django import forms
from myapps.general.models import Configuraciones


class ConfiguracionForm(forms.ModelForm):
	class Meta:
		model = Configuraciones

		fields = [
			'nombre',
			'tipo_cambio',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'nombre' : 'Nombre de la Empresa',
			'tipo_cambio' : 'Tipo de Moneda',
		}

		widgets = {
			'nombre' : forms.TextInput(attrs={'class':'form-control'}),
			'tipo_cambio' : forms.Select(attrs={'class':'form-control'}),
		}

