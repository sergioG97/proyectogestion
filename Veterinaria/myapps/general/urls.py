from django.conf.urls import url

from myapps.general.views import calcular_total
from django.contrib.auth.decorators import login_required

from myapps.general.views import ConfiUpdate
#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$',  login_required(calcular_total), name='resumen_datos'),
    url(r'^parametrizacion/(?P<pk>\d+)$',  login_required(ConfiUpdate.as_view()), name='configuracion'),

]