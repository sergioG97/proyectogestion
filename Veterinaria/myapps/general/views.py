# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, CreateView, UpdateView, DeleteView


from myapps.inventario.models import Inventario
from myapps.clientes.models import Clientes, Mascota
from myapps.planilla.models import Empleados
from myapps.servicios.models import Servicios

from myapps.general.models import Configuraciones
from myapps.general.forms import ConfiguracionForm
# Create your views here.

#Funcion que calcula el total de insumos, clientes, mascotas, empleados y servicios en la BD
class ConfiUpdate(UpdateView):
	model = Configuraciones
	form_class = ConfiguracionForm
	template_name = 'base/parametrizacion.html'
	success_url = reverse_lazy('general:resumen_datos')


def nombrePagina():
	Confi = Configuraciones.objects.get(pk=1)
	return Confi.nombre

def calcular_total(request):
	i = 0
	inventario = Inventario.objects.all()
	for x in inventario:
		i = i +1
	c = 0
	clientes = Clientes.objects.all()
	for x in clientes:
		c = c +1
	m = 0
	mascotas = Mascota.objects.all()
	for x in mascotas:
		m = m+1
	s = 0
	servicios = Servicios.objects.all()
	for x in servicios:
		s = s+1
	e = 0
	empleados = Empleados.objects.all()
	for x in empleados:
		e = e + 1

	nom = Configuraciones.objects.get(pk=1)

	contexto = {'insumos':i,'clientes':c,'mascotas':m,'servicios':s,'empleados':e,'nombreE':nom.nombre}
	return render(request,'base/index.html',contexto)
