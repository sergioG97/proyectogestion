from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.inventario.views import index, inventario_create ,InventarioList, InventarioCreate,\
 InventarioUpdate, inventario_total, get_precio_ajax

#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$', inventario_total),
    #Expresion regular, Vista , nombre de la vista
    url(r'^nuevo', login_required(InventarioCreate.as_view()), name='inventario_crear'),
    url(r'^listar', login_required(InventarioList.as_view()), name='inventario_listar'),
    url(r'^editar/(?P<pk>\d+)$',login_required(InventarioUpdate.as_view()), name='inventario_editar'),
    url(r'^get_precio_ajax', login_required(get_precio_ajax), name='get_precio'),
]