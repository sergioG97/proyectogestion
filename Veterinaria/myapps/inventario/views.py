# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.core.urlresolvers import reverse_lazy
#Para vistas basadas en clases
from myapps.inventario.models import Inventario
from myapps.inventario.forms import InventarioForm, InventarioEditForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView

from myapps.general.models import Configuraciones

from decimal import Decimal
# Create your views here.

#Vista de prueba
def index(request):
	return render(request,'base/base.html')

def inventario_list(request):
	inventario = Inventario.objects.all().order_by('id')#para odenar por id :)
	contexto = {'inventario':inventario}
	return render(request,'inventario/inventario_list.html',contexto)

#Vista para devolver el total de insumos
def inventario_total(request):
	i = 0
	inventario = Inventario.objects.all().order_by('id')#para odenar por id :)d
	for x in inventario:
		i = i +1
	contexto = {'total':i}
	return render(request,'inventario/index.html',contexto)

def inventario_create(request):
	if request.method == 'POST':
		form = InventarioForm(request.POST)
		if form.is_valid():
			form.save()
		return redirect('inventario:inventario_listar')
	else:
		form = InventarioForm()

	return render(request, 'inventario/inventario_form.html',{'form':form})

#Vista para crear insumos en Inventario
class InventarioCreate(CreateView):
	model = Inventario
	form_class = InventarioForm
	template_name = 'inventario/inventario_form.html'
	success_url = reverse_lazy('inventario:inventario_listar')

#Vista para listar los insumos del Inventario
class InventarioList(ListView):
	model = Inventario
	template_name = 'inventario/inventario_list.html'

#Vista para editar los insumos del Inventario
class InventarioUpdate(UpdateView):
	model = Inventario
	form_class = InventarioEditForm
	template_name = 'inventario/inventario_update.html'
	success_url = reverse_lazy('inventario:inventario_listar')

#Vista para devolver las existencias de un insumo
def existencias_Inventario(id):
	consulta = Inventario.objects.filter(pk=id)
	insumo = consulta.iterator().next()
	return insumo.cantidad

#Vista para descargar de inventario
def descargar_Inventario(id,cantidad):
	consulta = Inventario.objects.filter(pk=id)
	insumo = consulta.iterator().next()
	insumo.cantidad = insumo.cantidad - cantidad
	insumo.save()

def get_precio_ajax(request):
	Id = request.GET.get('id')
	insumo = Inventario.objects.get(pk=Id)
	valor = tipo_moneda(insumo.precio)
	response = JsonResponse({'precio': valor})
	return HttpResponse(response.content)

def tipo_moneda(precio):
	tipo = Configuraciones.objects.get(pk=1)
	tipo = tipo.tipo_cambio
	if tipo == "Q":
		print "Quetzales"
		return precio
	if tipo == "D":
		print "Dolares"
		return round((precio / Decimal(7.72)),2)
	if tipo == "E":
		print "Euros"
		return round((precio / Decimal(8.87)),2)
	else:
		return precio



