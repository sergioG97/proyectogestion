from django import forms
from myapps.inventario.models import Inventario

#Formulario de Inventario
#Este formulario es para ingresar nuevos insumos a la tabla Inventario
#Importante para el nombre de esta clase la nomenclatura sera Nombre del Modelo - Form
class InventarioForm(forms.ModelForm):
	class Meta:
		model = Inventario

		#Campos del Modelo
		fields = [
			'codigo',
			'nombre',
			'descripcion',
			'precio',
			'cantidad',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'codigo' : 'Codigo',
			'nombre' : 'Nombre',
			'descripcion' : 'Descripcion',
			'precio' : 'Precio',
			'cantidad' : 'Cantidad',
		}

		#Tipos de Campos para ingresar datos ej: TextBox, Combobox, checkbox, etc
		#Va el nombre del campo : Tipo de campo
		widgets = {
			'codigo' : forms.TextInput(attrs={'class':'form-control','value':'I-'}),
			'nombre' : forms.TextInput(attrs={'class':'form-control',}),
			'descripcion' : forms.Textarea(attrs={'class':'form-control'}),
			'precio' : forms.NumberInput(attrs={'class':'form-control','min' :  '0','placeholder':'Q.'}),
			'cantidad' : forms.NumberInput(attrs={'class':'form-control','min' :  '0'}),
		}


#Formulario para editar insumos del inventario
class InventarioEditForm(forms.ModelForm):
	class Meta:
		model = Inventario

		#Campos del Modelo
		fields = [
			'codigo',
			'nombre',
			'descripcion',
			'precio',
			'cantidad',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'codigo' : 'Codigo',
			'nombre' : 'Nombre',
			'descripcion' : 'Descripcion',
			'precio' : 'Precio',
			'cantidad' : 'Cantidad',
		}

		#Va el nombre del campo : Tipo de campo
		#El campo codigo no se va poder editar
		widgets = {
			'codigo' : forms.TextInput(attrs={'class':'form-control','readonly':'readonly'}),
			'nombre' : forms.TextInput(attrs={'class':'form-control'}),
			'descripcion' : forms.Textarea(attrs={'class':'form-control'}),
			'precio' : forms.NumberInput(attrs={'class':'form-control'}),
			'cantidad' : forms.NumberInput(attrs={'class':'form-control'}),
		}
