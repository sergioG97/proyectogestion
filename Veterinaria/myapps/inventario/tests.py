# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.test import TestCase
from myapps.inventario.models import Inventario
from myapps.inventario.forms import InventarioForm

# Create your tests here.

class InsumoInsertTest(TestCase):
	def setUp(self):
		self.insumo = Inventario.objects.create(
			codigo = "ABC",
			nombre = "Acondicionador",
			descripcion = "Adondicionador para perror",
			precio = 12.5,
			cantidad = 4
		)
	def test_get_insumos(self):
		insu = Inventario.objects.get(codigo="ABC")
		self.assertEqual(insu.nombre,"Acondicionador" )

class InsumoFormTest(TestCase):
	def test_registro(self):
		#Test para datos invalidos
		invalid_data = {
			'codigo' : 'BCD',
			'nombre' : 'Collar',
			'descripcion' : 'Collar para perro',
			'precio' : 'm',
			'cantidad' : '4'
		}

		form = InventarioForm(data=invalid_data)
		form.is_valid()
		self.assertTrue(form.errors)

		#Test para datos validos
		valid_data = {
			'codigo' : 'CDF',
			'nombre' : 'Collar',
			'descripcion' : 'Collar para perro',
			'precio' : '43.5',
			'cantidad' : 4
		}

		form = InventarioForm(data=valid_data)
		form.is_valid()
		self.assertFalse(form.errors)


