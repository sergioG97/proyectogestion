# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

#Modelo para la tabla Inventario
class Inventario(models.Model):
	codigo = models.CharField(max_length=45)
	nombre = models.CharField(max_length=45)
	descripcion = models.CharField(max_length=45)
	precio = models.DecimalField(max_digits = 7,decimal_places=2)
	cantidad = models.IntegerField()

	def __unicode__(self):
		return '{} {}'.format(self.codigo, self.nombre) #retornar el atributo


