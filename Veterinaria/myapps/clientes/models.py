# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

#Modelo para la tabla Clientes
class Clientes(models.Model):
	codigo = models.CharField(max_length = 45)
	nombres = models.CharField(max_length = 45)
	apellidos = models.CharField(max_length = 45)
	direccion = models.CharField(max_length = 45)
	telefono = models.CharField(max_length = 45)
	email = models.EmailField()
	numero_Mascotas = models.IntegerField(default = 0)

	def __unicode__(self):
		return '{} {}'.format(self.nombres, self.apellidos) #retornar el atributo

#Modelo para la tabla Mascota
class Mascota(models.Model):
	g = (('M','Macho'),('H','Hembra'))
	t = (('Canino','Canino'),('Felino','Felino'),('Ave','Ave'),('Reptil','Reptil'),('Roedor','Roedor'))
	codigo = models.CharField(max_length = 45)
	nombre = models.CharField(max_length = 45)
	raza = models.CharField(max_length = 45)
	edad = models.IntegerField()
	genero = models.CharField(max_length = 45,choices=g)
	tipo_mascota = models.CharField(max_length = 45,choices=t)
	estado = models.BooleanField()
	cliente = models.ForeignKey(Clientes, null=True, blank=True)

	def __unicode__(self):
		return '{} {}{}{}'.format(self.nombre, "(",self.codigo,")") #retornar el atributo