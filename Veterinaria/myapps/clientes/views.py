# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core.urlresolvers import reverse_lazy

from myapps.clientes.models import Clientes, Mascota
from myapps.carnet.models import Carnet
from myapps.clientes.forms import ClientesForm, MascotaForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# Create your views here.

#Vista de prueba
def index(request):
	return render(request,'clientes/index.html')

def cliente_list(request):
	clientes = clientes.objects.all().order_by('id')#para odenar por id :)
	contexto = {'clientes':clientes}
	return render(request,'clientes/clientes_list.html',contexto)

def cliente_create(request):
	if request.method == 'POST':
		form = ClientesForm(request.POST)
		if form.is_valid():
			item = form.save(commit=False)
			item.numero_Mascotas = 0
			item.save()
		return redirect('clientes:clientes_listar')
	else:
		form = ClientesForm()

	return render(request, 'clientes/clientes_form.html',{'form':form})

def matar_mascota(request,pk):
	mascota = Mascota.objects.get(pk=pk)
	mascota.estado = False
	mascota.save()
	return redirect('clientes:clientes_listar')


class ClienteCreate(CreateView):
	model = Clientes
	form_class = ClientesForm
	template_name = 'clientes/clientes_form.html'
	success_url = reverse_lazy('clientes:clientes_listar')

#listar clientes
class ClientesList(ListView):
	model = Clientes
	template_name = 'clientes/clientes_list.html'

#Mascotas
def pet_list(request,pk):
	mascotas = Mascota.objects.filter(cliente=pk).order_by('id')#para odenar por id :)
	c = mascotas.iterator().next()
	contexto = {'Mascotas':mascotas,'Cliente':c.cliente}
	return render(request,'clientes/mostrar_mascotas.html',contexto)

def pet_create(request):
	if request.method == 'POST':
		form = MascotaForm(request.POST)
		if form.is_valid():
			#form.estado = 1
			item = form.save(commit=False)
			item.estado = True
			item.save()
			cliente = Clientes.objects.filter(pk=item.cliente.id)
			c = cliente.iterator().next() 
			c.numero_Mascotas = c.numero_Mascotas + 1
			c.save()
		return redirect('clientes:clientes_listar')
	else:
		form = MascotaForm()

	return render(request, 'clientes/mascota_form.html',{'form':form})

#Vista para crear clientes


	#Vista para crear mascotas
class PetCreate(CreateView):
	model = Mascota
	form_class = MascotaForm
	template_name = 'clientes/mascota_form.html'
	success_url = reverse_lazy('clientes:mascota_listar')

#listar clientes
class PetList(ListView):
	model = Mascota
	template_name = 'clientes/mostrar_mascotas.html'

class PetEdit(UpdateView):
	model = Mascota
	form_class = MascotaForm
	template_name = 'clientes/mascota_form.html'
	success_url = reverse_lazy('clientes:clientes_listar')

class ClienteEdit(UpdateView):
	model = Clientes
	form_class = ClientesForm
	template_name = 'clientes/clientes_form.html'
	success_url = reverse_lazy('clientes:clientes_listar')
