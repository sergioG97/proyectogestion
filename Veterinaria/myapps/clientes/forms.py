from django import forms
from myapps.clientes.models import Clientes
from myapps.clientes.models import Mascota

#Formulario de cliente
#Formulario para ingresar nuevo cliente o mostrar datos del cliente
class ClientesForm(forms.ModelForm):
	class Meta:
		model = Clientes
		#campos
		fields = [
			'codigo',
			'nombres',
			'apellidos',
			'direccion',
			'telefono',
			'email',
		]

		#Labels
		labels = {
			'codigo' : 'Codigo',
			'nombres' : 'Nombres',
			'apellidos' : 'Apellidos',
			'direccion' : 'Direccion',
			'telefono' : 'Telefono',
			'email' : 'Correo electronico',
		}

		#Tipos de campos
		widgets = {
			'codigo' : forms.TextInput(attrs={'class':'form-control'}),
			'nombres' : forms.TextInput(attrs={'class':'form-control'}),
			'apellidos' : forms.TextInput(attrs={'class':'form-control'}),
			'direccion' : forms.TextInput(attrs={'class':'form-control'}),
			'telefono' : forms.NumberInput(attrs={'class':'form-control'}),
			'email' : forms.EmailInput(attrs={'class':'form-control'}),
		}


#Formulario de mascota. Se relaciona directamente con cliente.
#Formulario para ingresar nuevo cliente o mostrar datos del cliente
class MascotaForm(forms.ModelForm):
	class Meta:
		model = Mascota
		#campos
		fields = [
			'cliente',
			'codigo',
			'nombre',
			'tipo_mascota',
			'raza',
			'genero',
			'edad',
			'estado',
		]

		#Labels
		labels = {
			'cliente' : 'Cliente',
			'codigo' : 'Codigo',
			'nombre' : 'Nombre',
			'tipo_mascota' : 'Tipo',
			'raza' : 'Raza',
			'genero' : 'Genero',
			'edad' : 'Edad',
			'estado' : 'Estado',
		}

		#Tipos de campos
		widgets = {
			'cliente' : forms.Select(attrs={'class':'form-control'}),
			'codigo' : forms.TextInput(attrs={'class':'form-control'}),
			'nombre' : forms.TextInput(attrs={'class':'form-control'}),
			'tipo_mascota' : forms.Select(attrs={'class':'form-control'}),
			'raza' : forms.TextInput(attrs={'class':'form-control'}),
			'genero' : forms.Select(attrs={'class':'form-control'}),
			'edad' : forms.NumberInput(attrs={'class':'form-control'}),
			'estado' : forms.HiddenInput(),
		}