from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.clientes.views import index
from myapps.clientes.views import index, cliente_create ,ClientesList, ClienteCreate
from myapps.clientes.views import pet_create,PetList,PetCreate, pet_list, PetEdit, ClienteEdit,matar_mascota
#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$', index),
    url(r'^listar', login_required(ClientesList.as_view()), name='clientes_listar'),
    url(r'^nuevo', login_required(ClienteCreate.as_view()), name='cliente_crear'),
    url(r'^editar/(?P<pk>\d+)$',login_required(ClienteEdit.as_view()), name='cliente_editar'),
    url(r'^nuevamascota', login_required(pet_create), name='mascota_crear'),
    url(r'^mascota_editar/(?P<pk>\d+)$',login_required(PetEdit.as_view()), name='mascota_editar'),
    url(r'^mascotas_listar/(?P<pk>\d+)$', login_required(pet_list),name='mascota_listar'),
    url(r'^anular_mascota/(?P<pk>\d+)$', login_required(matar_mascota),name='matar_mascota'),
]