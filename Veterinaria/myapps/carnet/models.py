# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from myapps.clientes.models import Mascota
from myapps.servicios.models import Servicios

# Create your models here.

#Modelo para la tabla Carnet
class Carnet(models.Model):
	ciclo = models.CharField(max_length = 45)
	mascota = models.ForeignKey(Mascota,null=True,blank=True)
	def __unicode__(self):
		return 'Carnet: {}-{} {}'.format(self.mascota.codigo, self.ciclo, self.mascota.nombre) #retornar el atributo
	def codigoC(self):
		return 'Carnet: {}-{}'.format(self.mascota.codigo,self.ciclo)

#Modelo para la tabla DetalleCarnet
class DetalleCarnet(models.Model):
	carnet = models.ForeignKey(Carnet,null=True,blank=True)
	servicio = models.ForeignKey(Servicios,null=True,blank=True)
	fecha = models.DateField()
	estado = models.BooleanField()

