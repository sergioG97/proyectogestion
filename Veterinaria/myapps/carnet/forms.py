from django import forms
from django.contrib.admin.widgets import AdminDateWidget
from myapps.carnet.models import Carnet, DetalleCarnet
from myapps.servicios.models import Servicios
from myapps.clientes.models import Mascota

#Formulario para ingresar nuevos carnet
class CarnetForm(forms.ModelForm):
	class Meta:
		model = Carnet
		#Campos del Modelo
		fields = [
			'ciclo',
			'mascota',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'ciclo' : 'Ciclo',
			'mascota' : 'Mascota',
		}

		#Tipos de Campos para ingresar datos ej: TextBox, Combobox, checkbox, etc
		#Va el nombre del campo : Tipo de campo
		widgets = {
			'ciclo' : forms.TextInput(attrs={'class':'form-control'}),
			'mascota' : forms.Select(attrs={'class':'form-control'}),
			#'mascota' : forms.ModelChoiceField(queryset=Mascota.objects.all())
		}


#Formulario para programar un servicio futuro
class ServicioFuturoForm(forms.ModelForm):
	class Meta:
		DATE_INPUT_FORMATS = ['%Y-%m-%d']
		model = DetalleCarnet

		fields = [
			'carnet',
			'servicio',
			'fecha',
			'estado',
		]

		labels = {
			'carnet' : 'Carnet',
			'servicio' : 'Servicio',
			'fecha' : 'Fecha',
			'estado' : 'Cita Futura'
		}

		widgets = {
			'carnet' : forms.Select(attrs={'class':'form-control'}),
			'servicio' : forms.Select(attrs={'class':'form-control'}),
			#'fecha': forms.DateInput(attrs={'class':'form-control'}),
			'fecha' : forms.SelectDateWidget(),
			'estado' : forms.HiddenInput()
		}				

#Formulario para escoger los servicios
class ServicioMascotaForm(forms.ModelForm):
	class Meta:
		model = Servicios
		#revisar el attrs={'readonly':'readonly'}
		#Campos del Modelo
		fields = [
			'tipo_servicio',
			'precio',
		]

		labels = {
			'tipo_servicio' : 'Servicio',
			'precio' : 'Precio Q',
		}

		#Va el nombre del campo : Tipo de campo
		widgets = {
			'tipo_servicio' : forms.Select(attrs={'class':'form-control'}),
			'precio' : forms.TextInput(attrs={'class':'form-control','readonly':'readonly'}),
		}

#Formulario para escoger el carnet
class CarnetMascotaForm(forms.ModelForm):
	class Meta:
		model = Carnet

		#Campos del Modelo
		fields = [
			'mascota',
		]

		#Etiquetas que se mostraran el el formulario
		#Va el nombre del campo : Texto de la etiqueta
		labels = {
			'mascota' : 'Mascota',
		}

		#Tipos de Campos para ingresar datos ej: TextBox, Combobox, checkbox, etc
		#Va el nombre del campo : Tipo de campo
		widgets = {
			'mascota' : forms.Select(attrs={'class':'form-control'}),
		}


