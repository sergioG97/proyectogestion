from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from myapps.carnet.views import index, CarnetCreate, CarnetList, servicio_futuro, listar_proximasCitas,\
carnet_detalle, carnet_list

#URL's de la aplicacion
#Se listan todas las urls de la aplicacion
urlpatterns = [
    url(r'^$', index),
    url(r'^nuevo', login_required(CarnetCreate.as_view()), name='carnet_crear'),
    url(r'^listar', login_required(CarnetList.as_view()), name='carnet_listar'),
    url(r'^carnets_mascotas/(?P<pk>\d+)$', login_required(carnet_list), name='mascota_carnets'),
    url(r'^detalle_todo/(?P<pk>\d+)$', login_required(carnet_detalle), name='carnet_detalle_todo'),
    url(r'^programar_cita', login_required(servicio_futuro), name='carnet_proximaCita'),
    url(r'^citas_programadas/(?P<pk>\d+)$', login_required(listar_proximasCitas), name='carnet_citasProgramadas'),
]
