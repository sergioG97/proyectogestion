# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.core.urlresolvers import reverse_lazy

#Para vistas basadas en clases
from myapps.carnet.models import Carnet, DetalleCarnet
from myapps.clientes.models import Mascota
from myapps.carnet.forms import CarnetForm, ServicioFuturoForm
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
# Create your views here.

#Vista de prueba
def index(request):
	return HttpResponse('App de Carnet')


#vista para ingresar
class CarnetCreate(CreateView):
	model = Carnet
	form_class = CarnetForm
	template_name = 'carnet/carnet_form.html'
	success_url = reverse_lazy('carnet:carnet_listar')


#Vista para listar carnets
#Vista general
class CarnetList(ListView):
	model = Carnet
	template_name = 'carnet/carnet_list.html'

#Vista para listar carnets por un id de mascota
def carnet_list(request,pk):
	detalle = Carnet.objects.filter(mascota=pk).order_by('-ciclo')
	c = detalle.iterator().next()
	contexto = {'carnets':detalle,'mascota' : c.mascota}
	return render(request,'carnet/carnets_mascotas.html',contexto)

#Vista para listar todos los carnets segun una mascota
#Vista para listar todos los servicios en un carnet (Realizados y pendientes)
def carnet_detalle(request,pk):
	try:
		carnet = Carnet.objects.filter(pk=pk)
		detalle = DetalleCarnet.objects.filter(carnet=pk).order_by('-fecha')
	except DetalleCarnet.DoesNotExist:
		raise Http404("No existe detalle para este carnet")
	c = carnet.iterator().next()
	contexto = {'detalles':detalle,'carnet':c}
	return render(request,'carnet/detalle_carnet_list.html',contexto)
	
#Vista para listar los servicios que se han realizado 
def carnet_detalleRealizados(request,pk):
	try:
		detalle = DetalleCarnet.objects.get(carnet=pk,estado=1)#Servicios ya realizados
	except DetalleCarnet.DoesNotExist:
		raise Http404("No existe detalle para este carnet")
	contexto = {'detalles':detalle}
	return render(request,'mascota/mascota_list.html',contexto)

#Vistas para citas programadas (servicios futuros)
def servicio_futuro(request):
	if request.method == 'POST':
		form = ServicioFuturoForm(request.POST)
		#form.estado = False
		if form.is_valid():
			item = form.save(commit=False)
			item.estado = False
			item.save()
		return redirect('carnet:carnet_listar')
	else:
		form = ServicioFuturoForm()
	return render(request, 'carnet/servicio_futuro_form.html',{'form':form})

def listar_proximasCitas(request,pk):
	try:
		detalle = DetalleCarnet.objects.filter(carnet=pk,estado=0).order_by('fecha')
	except DetalleCarnet.DoesNotExist:
		raise Http404("Error al obtener las citas")
	contexto = {'detalles':detalle}
	return render(request,'carnet/citas_futuras.html',contexto)


#Vista para crear un servicio ya echo
def servicio_nuevo_echo(obVenta,obDetalle):
	citaCarnet = DetalleCarnet(
		carnet=obVenta.carnet,
		servicio=obDetalle.servicio,
		estado=True,
		fecha=obVenta.fecha,
	)
	citaCarnet.save()